FROM python:3.8

WORKDIR /
ADD requirements.txt /requirements.txt
ADD src/run.py /script/run.py
RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python"]
CMD ["/script/run.py"]
