#  media-url-update-service-xml
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import logging
import sys

import mysql
import mysql.connector as mariadb
import requests
import xml.etree.ElementTree as ET

from requests import RequestException

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="[%(levelname)s] [%(name)s] %(message)s",
)


def update_records_in_database(records_to_update, cursor, connection):
    for key in records_to_update:
        record = records_to_update[key]
        try:
            # first, get memobase-sig from external sig:
            sql_stmt = (
                "SELECT sig FROM entities "
                + 'WHERE sig LIKE "%'
                + record["originalId"]
                + '-%" '
                + 'AND NOT sig LIKE "%'
                + record["originalId"]
                + '-%-%"'
            )
            logging.debug('executing "' + sql_stmt + '"')
            cursor.execute(sql_stmt)
            entry = cursor.fetchall()
            if len(entry) == 1:
                sig = entry[0][0]
            elif len(entry) == 0:
                logging.error(
                    f"No entry in database for: {record['institutionId']}/{record['originalId']}"
                )
                continue
            else:
                logging.error(
                    "Database contains more than one entry for identifier: "
                    + record["institutionId"]
                    + "/"
                    + record["originalId"]
                )
                continue

            # then, update tables:
            sql_stmt = (
                "UPDATE entities "
                + 'SET uri = "'
                + record["loc"]
                + '" '
                + ", lastchange = NOW() "
                + ", notifiedon = NULL "
                + 'WHERE sig = "'
                + sig
                + '"'
            )
            logging.debug('executing "' + sql_stmt + '"')
            cursor.execute(sql_stmt)
            sql_stmt = (
                "UPDATE metadata "
                + "SET modificationtime = NOW() "
                + 'WHERE sig = "'
                + sig
                + '"'
            )
            logging.debug('executing "' + sql_stmt + '"')
            cursor.execute(sql_stmt)
        except mysql.connector.Error as ex:
            logging.error(f"MySQL Connection Error: {ex}.")
        except Exception as ex:
            logging.error(f"Unknown Exception: {ex}")
            raise ex
    connection.commit()


def connect_to_database():
    try:
        connection = mariadb.connect(
            user=os.environ["MARIADB_USER"].rstrip(),
            password=os.environ["MARIADB_PASSWORD"].rstrip(),
            host=os.environ["MARIADB_HOST"],
            port=os.environ["MARIADB_PORT"],
            database=os.environ["MARIADB_DATABASE"],
            ssl_ca=os.environ["MARIADB_HOST_CERTS"],
            ssl_verify_cert=True,
        )
        connection.autocommit = False
        cursor = connection.cursor()
    except mysql.connector.Error as ex:
        logging.error(f"MySQL Connection Error: {ex}.")
        raise ex
    except Exception as ex:
        logging.error(f"Unknown Exception: {ex}")
        raise ex
    return cursor, connection


def revise_entries(data: bytes):
    revised_xml_data = dict()
    response_xml = ET.fromstring(data.decode("utf-8"))
    ns = {"d": "http://www.schemas.memobase.iais.fraunhofer.de/sitemap/1.0"}
    urls = response_xml.findall("d:url", ns)
    for url in urls:
        entry = {
            "loc": url.find("d:loc", ns).text,
            "originalId": url.find("d:originalId", ns).text,
            "priority": url.find("d:priority", ns).text,
            "institutionId": url.find("d:institutionId", ns).text,
            "lastmod": url.find("d:lastmod", ns).text,
        }

        # Spezialfall Datenbereinigung RadioX
        if entry["institutionId"] == "RadioX":
            entry["originalId"] = (
                entry["originalId"][:-4] + "_" + entry["originalId"][-3:]
            )

        key = entry["originalId"]
        if key in revised_xml_data:
            existing_entry = revised_xml_data[key]
            if float(existing_entry["priority"]) < float(entry["priority"]):
                logging.info(
                    f"Overwrite entry with lower priority with "
                    f'identifier "{key}". New entry: {entry}'
                )
                revised_xml_data[key] = entry
        else:
            logging.info(f'Add new entry for identifier "{key}". New entry: {entry}.')
            revised_xml_data[key] = entry
    return revised_xml_data


mariadb_cursor, mariadb_connection = connect_to_database()
xml_urls = os.environ["XML_URIS"].split(",")
for xml_url in xml_urls:
    try:
        response = requests.get(xml_url)
        if response.ok:
            logging.info(f"Loaded XML sitemap from {xml_url}.")
            xml_data = response.content
            fixed_xml_data = revise_entries(xml_data)
            update_records_in_database(
                fixed_xml_data, mariadb_cursor, mariadb_connection
            )
        else:
            logging.error(
                f"Could not load file from {xml_url} because "
                f"of {response.reason} ({response.status_code})"
            )
    except RequestException as req_ex:
        logging.error(f"Request Error: {req_ex} for url {xml_url}.")
