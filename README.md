### Media URL XML Sitemap Update Job

This job fetches the XML sitemaps provided by institutions to update the media links in the database.

#### Configuration
- `XML_URIS`: Download urls of the sitemap files in a comma separated list.
- `MARIADB_*`: Maria DB Settings.

[Confluence Documentation](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/1929445377/Service+Media+URL+Update+XML)